appname = aa-moonmining
package = moonmining

help:
	@echo "Makefile for $(appname)"

makemessages:
	cd $(package) && \
	django-admin makemessages \
		-l de \
		-l en \
		-l es \
		-l fr_FR \
		-l it_IT \
		-l ja \
		-l ko_KR \
		-l ru \
		-l uk \
		-l zh_Hans \
		--keep-pot \
		--ignore 'build/*'

tx_push:
	tx push --source

tx_pull:
	tx pull -f

compilemessages:
	cd $(package) && \
	django-admin compilemessages \
		-l de \
		-l en \
		-l es \
		-l fr_FR \
		-l it_IT \
		-l ja \
		-l ko_KR \
		-l ru \
		-l uk \
		-l zh_Hans

coverage:
	# coverage run ../myauth/manage.py test $(package) --keepdb --failfast && coverage html && coverage report -m
	coverage run --concurrency=multiprocessing ../myauth/manage.py test --keepdb --failfast --timing --parallel --exclude-tag=exclude-parallel && coverage combine && coverage html && coverage report -m

pylint:
	pylint --load-plugins pylint_django $(package)

graph_models:
	python ../myauth/manage.py graph_models $(package) --arrow-shape normal -o $(appname)_models.png

create_testdata:
	python ../myauth/manage.py test $(package).tests.testdata.create_eveuniverse --keepdb -v 2
