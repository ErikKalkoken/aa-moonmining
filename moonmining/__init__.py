"""Alliance Auth app for tracking moon extractions and scouting new moons."""

# pylint: disable = invalid-name
default_app_config = "moonmining.apps.MoonPlanerConfig"

__version__ = "1.11.1"
__title__ = "Moon Mining"
